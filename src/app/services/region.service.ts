import { HttpClient } from '@angular/common/http';
import { Injectable, effect, inject, signal } from '@angular/core';
import { Observable, map, tap } from 'rxjs';
import Region, { RegionDTO } from '../interfaces/region';

@Injectable({
  providedIn: 'root'
})
export class RegionService {

  REGIONS_URL = 'https://geo.api.gouv.fr/regions?fields=nom,code';
  selectionRegionSig = signal<Region>({Code: '-1', Name: ''} );
  private departmentEffect = effect(() => {
    console.log(this.selectionRegionSig())
  })
  http = inject(HttpClient);

 
getRegions(): Observable<Region[]> {
  return this.http
  .get<RegionDTO[]>(this.REGIONS_URL)
  .pipe(
    tap((data) => console.log('a:', data)),
    map((data:RegionDTO[])=> this.changeFields(data)),
    tap(data => console.log(data))
  );
}
changeFields(data:RegionDTO[]): Region[] {
const regions: Region[] = []
for (let index = 0; index < data.length; index++) {
  const reg: Region = {
    Name: data[index].nom,
    Code: data[index].code,  
};
regions.push(reg);
}
return regions
}
}