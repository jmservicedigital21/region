import { Component, OnDestroy, OnInit, inject, signal } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { RegionService } from './services/region.service';
import { Observable, Subscription, tap } from 'rxjs';
import Region from './interfaces/region';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, CommonModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent implements OnInit, OnDestroy{
  
  ngOnInit(): void {

    this.regionsService
    .getRegions()
    .pipe(
      tap((data)=> this.regionsSig.set(data)))
    .subscribe() ; 


    
  }
  
  
  title = 'region';
  // regions$ = new Observable<Region[]>();
  regionsSig = signal<Region[]>([]);
  regionsService = inject(RegionService);
  selectionRegionSig = this.regionsService.selectionRegionSig;
  subscription !: Subscription;

  selectRegion(region : Region){
    this.selectionRegionSig.set(region);

  }

ngOnDestroy(): void {
  this.subscription.unsubscribe
}
}
